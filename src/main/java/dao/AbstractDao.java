package dao;

/**
 * @author Turdean Bogdan
 * @since May 12, 2021
 */

import connection.ConnectionFactory;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AbstractDao<T> {
    protected static final Logger LOGGER = Logger.getLogger(AbstractDao.class.getName());
    /**
     * Variabila generica pentru clasa cu care se va instantia
     */
    private final Class<T> type;

    @SuppressWarnings("unchecked")
    public AbstractDao() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

    }

    /**
     * Functie pentru crearea unu query care selecteaza dintr-un tabel la un anumit ID
     * @param field campul dupa care se face cautarea(ID-ul)
     * @return returneaza un string corespunzator instructiunii SQL
     */
    private String createSelectQuery(String field) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE " + field + " =?");
        return sb.toString();
    }
    /**
     * Functie pentru crearea unu query care sterge o anumita linie dintr-un tabel
     * @param field campul dupa care se face cautarea(ID-ul)
     * @return returneaza un string corespunzator instructiunii SQL
     */
    private String createDeleteQuery(String field) {
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE " + field + " =?");
        return sb.toString();
    }

    /**
     * Functie pentru crearea unui query ce selecteaza intreg continutul unui tabel
     * @return returneaza un string corespunzator instructiunii SQL
     */
    private String createFindQuery() {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName());
        return sb.toString();
    }
    /**
     * Functie pentru crearea unui query ce insereaza o linie noua intr-un tabel
     * @return returneaza un string corespunzator instructiunii SQL
     */
    private String createInsertQuery(){
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ");
        sb.append(type.getSimpleName());
        sb.append(" VALUES ");
        sb.append(" (?, ?, ?, ?) ");
        return sb.toString();
    }
    /**
     * Functie pentru crearea unui query ce actualizeaza o linie dintr-un tabel
     * @param field1 campul dupa care se selecteaza linia dorita(ID-ul)
     * @param field2 primul camp ce va fi actualizat
     * @param field3 al doilea camp ce va fi actualizat
     * @param field4 al treilea camp ce va fi actualizat
     * @return returneaza un string corespunzator instructiunii SQL
     */
    private String createUpdateQuery(String field1, String field2, String field3, String field4){
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ");
        sb.append(type.getSimpleName());
        sb.append(" SET ");
        sb.append(field2 + "=? , " + field3 + "=? , " + field4 + "=?");
        sb.append(" WHERE " + field1 + "=?");
        return sb.toString();
    }

    /**
     * Functie pentru returnarea intreg continutului unui tabel
     * @return returneaza lista de obiecte daca exista sau null daca nu exista
     */
    public List<T> findAll() {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createFindQuery();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();

            return createObjects(resultSet);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * Functie ce returneaza un obiect dintr-un tabel
     * @param id identificatorul obiectului pe care dorim sa il returnam
     * @return returneaza obiectul din tabel daca acesta exista si null daca nu exista
     */
    public T findById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery("id");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();

            return createObjects(resultSet).get(0);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * Functie ce returneaza lista de obiecte
     * Se creeaza instante ale obiectelor ce sunt adaugate apoi intr-o lista
     * @param resultSet lista rezultatelor
     * @return returneaza lista de obiecte sau null daca nu exista
     */
    private List<T> createObjects(ResultSet resultSet) {
        List<T> list = new ArrayList<T>();
        Constructor[] ctors = type.getDeclaredConstructors();
        Constructor ctor = null;
        for (int i = 0; i < ctors.length; i++) {
            ctor = ctors[i];
            if (ctor.getGenericParameterTypes().length == 0)
                break;
        }
        try {
            while (resultSet.next()) {
                ctor.setAccessible(true);
                T instance = (T)ctor.newInstance();
                for (Field field : type.getDeclaredFields()) {
                    String fieldName = field.getName();
                    Object value = resultSet.getObject(fieldName);
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(fieldName, type);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance, value);
                }
                list.add(instance);
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IntrospectionException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * Functie ce insereaza un obiect intr-un tabel
     * @param t obiectul ce urmeaza sa fie adaugat
     * @return returneaza valoarea lui executeUpdate sau -1 daca execute a esuat
     */
    public int insert(T t) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createInsertQuery();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            int i = 1;
            for(Field field: t.getClass().getDeclaredFields()){
                field.setAccessible(true);
                Object value = field.get(t);
                statement.setObject(i, value);
                i++;
            }

            return statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
        } catch (IllegalAccessException e){
            e.printStackTrace();
        }
        finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return -1;
    }

    /**
     * Functie pentru actualizarea unui obiect dintr-un tabel
     * @param t obiectul ce urmeaza sa fie actualizat(instantiat cu constructor fara ID)
     * @param id id-ul obiectului ce urmeaza sa fie actualizat
     * @return returneaza valoarea lui executeUpdate sau -1 daca execute a esuat
     */
    public int update(T t, int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String field1 = null, field2 = null, field3 = null, field4 = null;
        int i = 1;
        for(Field field: t.getClass().getDeclaredFields()){
            switch(i){
                case 1:
                    field1 = field.getName();
                    break;
                case 2:
                    field2 = field.getName();
                    break;
                case 3:
                    field3 = field.getName();
                    break;
                case 4:
                    field4 = field.getName();
                    break;
            }
            i++;
        }
        String query = createUpdateQuery(field1, field2, field3, field4);
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(4, id);
            i = 1;
            for (Field f : t.getClass().getDeclaredFields()) {
                f.setAccessible(true);
                Object value = f.get(t);
                if (i != 1)
                    statement.setObject(i - 1, value);
                i++;
            }
            return statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
        } catch (IllegalAccessException e){
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return -1;
    }

    public int deleteById(int id) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createDeleteQuery("id");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);

            return statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return -1;
    }
}
