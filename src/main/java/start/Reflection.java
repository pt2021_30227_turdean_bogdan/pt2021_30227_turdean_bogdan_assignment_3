package start;

/**
 * @author Turdean Bogdan
 * @since May 12, 2021
 */

import java.lang.reflect.Field;

public class Reflection {
    /**
     * Metoda pentru afisarea in consola a unui obiect oarecare
     * @param object obiectul a carui campuri vor fi afisate
     */

    public static void retrieveProperties(Object object) {

        for (Field field : object.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            Object value;
            try {
                value = field.get(object);
                System.out.println(field.getName() + "=" + value);

            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        }
    }
}
