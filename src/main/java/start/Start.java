package start;

/**
 * @author Turdean Bogdan
 * @since May 12, 2021
 */

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import bll.*;
import dao.ComandaDao;
import dao.ProdusDao;
import model.Client;
import model.Comanda;
import model.Produs;
import presentation.Controller;
import presentation.View;


public class Start {
    protected static final Logger LOGGER = Logger.getLogger(Start.class.getName());

    /**
     * Metoda main
     * @param args argumentele functiei main
     */
    public static void main(String[] args) throws SQLException {
        View view = new View();
        Controller controller = new Controller(view);
        view.setVisible(true);

    }
}
