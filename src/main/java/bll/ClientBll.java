package bll;

/**
 * @author Turdean Bogdan
 * @since May 12, 2021
 */

import dao.ClientDao;
import model.Client;
import presentation.View;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class ClientBll {
    /**
     * Lista validatoarelor unui client
     */
    private List<Validator<Client>> validators;
    /**
     * obiect pentru apelarea functiilor de insert, update etc.
     */
    private ClientDao clientDao;
    /**
     * Fereastra de unde se preiau datele
     */
    private View view;

    public ClientBll(View view){
        validators = new ArrayList<Validator<Client>>();
        validators.add(new EmailValidator());
        validators.add(new ClientAgeValidator());
        this.view = view;
        clientDao = new ClientDao();
    }

    /**
     * Functie ce cauta un client dupa un anumit id
     * @param id id-ul dupa care se cauta
     * @return returneaza clientul daca exista
     */
    public Client findClientById(int id){
        Client client = null;
        client = clientDao.findById(id);
        if(client == null){
            JOptionPane.showMessageDialog(this.view, "The client with id =" + id + " was not found!");
            throw new NoSuchElementException("The client with id =" + id + " was not found!");
        }
        return client;
    }

    /**
     * Functie pentru stergerea unui client
     * @param id id-ul clientului sters
     */
    public void deleteClientById(int id){
        int client = clientDao.deleteById(id);
        if(client == -1){
            JOptionPane.showMessageDialog(this.view, "The client with id =" + id + " was not found!");
            throw new NoSuchElementException("The client with id =" + id + " was not found!");
        }
    }

    /**
     * Functie ce afiseaza toti clientii din tabel
     * @return returneaza lista de clienti daca exista
     */
    public List<Client> findAllClients(){
        List<Client> clients = clientDao.findAll();
        if(clients == null){
            JOptionPane.showMessageDialog(this.view, "Nu s-au gasit clienti");
            throw new NoSuchElementException("Nu s-au gasit clienti");
        }
        return clients;
    }

    /**
     * Functie ce insereaza un client in tabel
     * @param client obiectul ce urmeaza sa fie adaugat
     */
    public void insertClient(Client client){
        List<Client> clients = clientDao.findAll();
        for(Client c: clients){
            if(c.getId() == client.getId()){
                JOptionPane.showMessageDialog(this.view, "The client with id = " + client.getId() + " already exists");
                throw new NoSuchElementException("The client with id = " + client.getId() + " already exists");
            }
        }
        validators.get(0).validate(client);
        validators.get(1).validate(client);
        if(clientDao.insert(client) == -1){
            JOptionPane.showMessageDialog(this.view, "Clientul nu s-a putut introduce");
            throw new NoSuchElementException("Clientul nu s-a putut introduce");
        }
    }

    /**
     * Functie ce actualizeaza un client
     * @param id id-ul clientului ce urmeaza sa fie actualizat
     * @param client un obiect ce contine noile campuri pentru a actualiza clientul
     */
    public void update(int id, Client client){
        List<Client> clients = findAllClients();
        boolean exist = false;
        for(Client c: clients){
            if(c.getId() == id){
                exist = true;
            }
        }
        if(exist = false){
            JOptionPane.showMessageDialog(this.view, "Client inexistent");
            throw new NoSuchElementException("Client inexistent");
        }
        validators.get(0).validate(client);
        validators.get(1).validate(client);
        if(clientDao.update(client, id) == -1){
            JOptionPane.showMessageDialog(this.view, "Nu sa putut face update");
            throw new NoSuchElementException("Nu sa putut face update");
        }
    }
}
