package bll;
/**
 * @author Turdean Bogdan
 * @since May 12, 2021
 */
import dao.ClientDao;
import dao.ComandaDao;
import dao.ProdusDao;
import model.Client;
import model.Comanda;
import model.Produs;
import presentation.View;

import javax.swing.*;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;

public class ComandaBll {
    /**
     * Obiect pentru accesarea functiilor insert, update etc.
     */
    private ComandaDao comandaDao;
    /**
     * Obiect pentru accesarea functiilor insert, update etc. a produselor
     */
    private ProdusDao produsDao;
    /**
     * Obiect pentru accesarea functiilor insert, update etc. a clientilor
     */
    private ClientDao clientDao;
    /**
     * interfata grafica de unde se preiau date
     */
    private View view;

    public ComandaBll(View view) {
        comandaDao = new ComandaDao();
        produsDao = new ProdusDao();
        clientDao = new ClientDao();
        this.view = view;
    }

    /**
     * Functie ce cauta o anumita comanda
     * @param id id-ul comenzii cautate
     * @return returneaza comanda daca exista
     */
    public Comanda findComandaById(int id){
        Comanda comanda = comandaDao.findById(id);
        if(comanda == null){
            JOptionPane.showMessageDialog(this.view, "The command with id =" + id + " was not found!");
            throw new NoSuchElementException("The command with id =" + id + " was not found!");
        }
        return comanda;
    }

    /**
     * Functie ce sterge o anumita comanda
     * @param id id-ul comenzii
     */
    public void deleteComandaById(int id){
        Comanda c = null;
        c =comandaDao.findById(id);
        List<Produs> produse = produsDao.findAll();
        if(c != null){
            for(Produs p: produse){
                if(p.getId() == c.getIdProdus()){
                    ProdusBll produsBll = new ProdusBll(this.view);
                    produsBll.update(p.getId(), new Produs(p.getName(), p.getPrice(), p.getCantity() + c.getCantitate()));
                    p.setCantity(p.getCantity() + c.getCantitate());
                }
            }
        }
        int comanda = comandaDao.deleteById(id);
        if(comanda == -1){
            JOptionPane.showMessageDialog(this.view, "The command with id =" + id + " was not found!");
            throw new NoSuchElementException("The command with id =" + id + " was not found!");
        }
        try{
            FileWriter f = new FileWriter("D:\\Proiecte\\AN 2\\SEM2\\TP\\PT2021_30227_Turdean_Bogdan_Assignment_3\\src\\main\\java\\" + id + ".txt");
            f.write("");

            f.close();
        } catch (IOException ex){
            ex.printStackTrace();
        }
    }

    /**
     * Functie ca afiseaza toate comenzile
     * @return returneaza lista de comenzi daca exista
     */
    public List<Comanda> findAllCommands(){
        List<Comanda> comenzi = comandaDao.findAll();
        if(comenzi == null){
            JOptionPane.showMessageDialog(this.view, "Nu s-au gasit comenzi");
            throw new NoSuchElementException("Nu s-au gasit comenzi");
        }
        return comenzi;
    }

    /**
     * Functie ce insereaza o comanda noua
     * @param comanda comanda ce urmeaza sa fie inserata
     */
    public void insertComanda(Comanda comanda){
        List<Comanda> comenzi = comandaDao.findAll();
        for(Comanda c: comenzi){
            if(c.getId() == comanda.getId()){
                JOptionPane.showMessageDialog(this.view, "The command with id = " + comanda.getId() + " already exists");
                throw new NoSuchElementException("The command with id = " + comanda.getId() + " already exists");
            }
        }

        List<Produs> produse = produsDao.findAll();
        boolean exist = false;
        for(Produs p: produse){
            if(p.getId() == comanda.getIdProdus()) {
                exist = true;
                if(p.getCantity() < comanda.getCantitate()){
                    JOptionPane.showMessageDialog(this.view, "Cantitate ceruta prea mare");
                    throw new NoSuchElementException("Cantitate ceruta prea mare");
                }
            }

        }
        if(exist == false){
            JOptionPane.showMessageDialog(this.view, "Produsul nu exista");
            throw new NoSuchElementException("Produsul nu exista");
        }

        List<Client> clienti = clientDao.findAll();
        exist = false;
        for(Client c: clienti){
            if(c.getId() == comanda.getIdClient())
                exist = true;
        }
        if(exist == false){
            JOptionPane.showMessageDialog(this.view, "Clientul nu exista");
            throw new NoSuchElementException("Clientul nu exista");
        }

        if(comandaDao.insert(comanda) == -1){
            JOptionPane.showMessageDialog(this.view, "Comanda nu s-a putut introduce");
            throw new NoSuchElementException("Comanda nu s-a putut introduce");
        }
        try{
            FileWriter f = new FileWriter("D:\\Proiecte\\AN 2\\SEM2\\TP\\PT2021_30227_Turdean_Bogdan_Assignment_3\\src\\main\\java\\" + comanda.getId() + ".txt");
            f.write("Comanda " + comanda.getId() + ": \n");
            for(Client c: clienti){
                if(c.getId() == comanda.getIdClient()){
                    f.write("Client: " + c.getName() + "\n");
                    break;
                }
            }
            for(Produs p: produse){
                if(p.getId() == comanda.getIdProdus()){
                    f.write("Comanda: " + p.getName() + " " + comanda.getCantitate() + " buc. * " + p.getPrice() + " lei total: " + comanda.getCantitate()*p.getPrice() + " lei");
                    break;
                }
            }
            f.close();
        } catch (IOException ex){
            ex.printStackTrace();
        }
        for(Produs p: produse){
            if(p.getId() == comanda.getIdProdus()){
                ProdusBll produsBll = new ProdusBll(this.view);
                produsBll.update(p.getId(), new Produs(p.getName(), p.getPrice(), p.getCantity() - comanda.getCantitate()));
                p.setCantity(p.getCantity() - comanda.getCantitate());
            }
        }
    }

    /**
     * Functie ce actualizeaza o comanda
     * @param id id-ul comenzii
     * @param comanda un obiect ce contine noile campurile pentru comanda
     */
    public void update(int id, Comanda comanda){
        List<Comanda> comenzi = findAllCommands();
        boolean exist = false;
        for(Comanda c: comenzi){
            if(c.getId() == id){
                exist = true;
            }
        }
        if(exist = false){
            JOptionPane.showMessageDialog(this.view, "Comanda inexistenta");
            throw new NoSuchElementException("Comanda inexistenta");
        }
        List<Produs> produse = produsDao.findAll();
        exist = false;
        for(Produs p: produse){
            if(p.getId() == comanda.getIdProdus()) {
                exist = true;
                if(p.getCantity() < comanda.getCantitate()){
                    JOptionPane.showMessageDialog(this.view, "Cantitate ceruta prea mare");
                    throw new NoSuchElementException("Cantitate ceruta prea mare");
                }
            }

        }
        if(!exist){
            JOptionPane.showMessageDialog(this.view, "Produsul nu exista");
            throw new NoSuchElementException("Produsul nu exista");
        }

        List<Client> clienti = clientDao.findAll();
        exist = false;
        for(Client c: clienti){
            if(c.getId() == comanda.getIdClient())
                exist = true;
        }
        if(exist == false){
            JOptionPane.showMessageDialog(this.view, "Clientul nu exista");
            throw new NoSuchElementException("Clientul nu exista");
        }
        for(Produs p: produse){
            if(p.getId() == comanda.getIdProdus()){
                ProdusBll produsBll = new ProdusBll(this.view);
                produsBll.update(p.getId(), new Produs(p.getName(), p.getPrice(), p.getCantity() - comanda.getCantitate()));
                p.setCantity(p.getCantity() + comanda.getCantitate());
            }
        }
        //validator.validate(comanda);
        if(comandaDao.update(comanda, id) == -1){
            JOptionPane.showMessageDialog(this.view, "Nu sa putut face update");
            throw new NoSuchElementException("Nu sa putut face update");
        }
        try{
            FileWriter f = new FileWriter("D:\\Proiecte\\AN 2\\SEM2\\TP\\PT2021_30227_Turdean_Bogdan_Assignment_3\\src\\main\\java\\" + id + ".txt");
            f.write("Comanda " + id + ": \n");
            for(Client c: clienti){
                if(c.getId() == comanda.getIdClient()){
                    f.write("Client: " + c.getName() + "\n");
                    break;
                }
            }
            for(Produs p: produse){
                if(p.getId() == comanda.getIdProdus()){
                    f.write("Comanda: " + p.getName() + " " + comanda.getCantitate() + " buc. * " + p.getPrice() + " lei total: " + comanda.getCantitate()*p.getPrice() + " lei");
                    break;
                }
            }

            f.close();
        } catch (IOException ex){
            ex.printStackTrace();
        }
        for(Comanda c: comenzi){
            if(comanda.getId() == c.getId()){
                for(Produs p: produse){
                    if(p.getId() == comanda.getIdProdus()){
                        ProdusBll produsBll = new ProdusBll(this.view);
                        produsBll.update(p.getId(), new Produs(p.getName(), p.getPrice(), p.getCantity() - c.getCantitate()));
                        p.setCantity(p.getCantity() - c.getCantitate());
                    }
                }
                break;
            }

        }

    }
}
