package bll;
/**
 * @author Turdean Bogdan
 * @since May 12, 2021
 */
public interface Validator <T>{
    public void validate(T t);
}
