package bll;

/**
 * @author Turdean Bogdan
 * @since May 12, 2021
 */

import model.Comanda;
import model.Produs;

public class CantitateValidator implements Validator<Produs>{

    /**
     * Functie ce valideaza cantitatea unui produs
     * @param produs produsul pentru care se face validarea
     */
    public void validate(Produs produs) {
        if(produs.getCantity() < 0){
            throw new IllegalArgumentException("Cannot have negative cantity");
        }
    }
}
