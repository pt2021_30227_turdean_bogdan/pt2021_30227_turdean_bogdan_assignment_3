package bll;
/**
 * @author Turdean Bogdan
 * @since May 12, 2021
 */
import dao.ClientDao;
import dao.ProdusDao;
import model.Client;
import model.Produs;
import presentation.View;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class ProdusBll {
    /**
     * Validatorul pentru produs
     */
    private Validator validator;
    /**
     * Obiect pentru a accesa functiile insert, update etc.
     */
    private ProdusDao produsDao;
    /**
     * GUI de unde se preiau datele
     */
    private View view;

    public ProdusBll(View view){
        validator = new CantitateValidator();
        this.view = view;
        produsDao = new ProdusDao();
    }

    /**
     * Functie ce cauta un anumit produs
     * @param id id-ul produsului cautat
     * @return returneaza produsul daca exista
     */
    public Produs findProductById(int id){
        Produs produs = produsDao.findById(id);
        if(produs == null){
            JOptionPane.showMessageDialog(this.view, "The product with id =" + id + " was not found!");
            throw new NoSuchElementException("The product with id =" + id + " was not found!");
        }
        return produs;
    }

    /**
     * Functie ce sterge un anumit produs
     * @param id id-ul produsului ce trebuie sters
     */
    public void deleteProductById(int id){
        int produs = produsDao.deleteById(id);
        if(produs == -1){
            JOptionPane.showMessageDialog(this.view, "The product with id =" + id + " was not found!");
            throw new NoSuchElementException("The product with id =" + id + " was not found!");
        }
    }

    /**
     * Functie ce afiseaza toate produsele
     * @return returneaza lista de produse daca exista
     */
    public List<Produs> findAllProducts(){
        List<Produs> produse = produsDao.findAll();
        if(produse == null){
            JOptionPane.showMessageDialog(this.view, "Nu s-au gasit produse");
            throw new NoSuchElementException("Nu s-au gasit produse");
        }
        return produse;
    }

    /**
     * Functie ce insereaza un nou produs
     * @param produs produsul ce urmeaza sa fie inserat
     */
    public void insertProdus(Produs produs){
        List<Produs> produse = produsDao.findAll();
        for(Produs c: produse){
            if(c.getId() == produs.getId()){
                JOptionPane.showMessageDialog(this.view, "The product with id = " + produs.getId() + " already exists");
                throw new NoSuchElementException("The product with id = " + produs.getId() + " already exists");
            }
        }
        validator.validate(produs);
        if(produsDao.insert(produs) == -1){
            JOptionPane.showMessageDialog(this.view, "Produsul nu s-a putut introduce");
            throw new NoSuchElementException("Produsul nu s-a putut introduce");
        }
    }

    /**
     * Functie ce actualizeaza un produs
     * @param id id-ul produsului ce trebuie actualizat
     * @param produs obiect ce contine noile campuri pentru produs
     */
    public void update(int id, Produs produs){
        List<Produs> products = findAllProducts();
        boolean exist = false;
        for(Produs c: products){
            if(c.getId() == id){
                exist = true;
            }
        }
        if(exist = false){
            JOptionPane.showMessageDialog(this.view, "Produs inexistent");
            throw new NoSuchElementException("Produs inexistent");
        }
        validator.validate(produs);
        if(produsDao.update(produs, id) == -1){
            JOptionPane.showMessageDialog(this.view, "Nu sa putut face update");
            throw new NoSuchElementException("Nu sa putut face update");
        }
    }
}
