package bll;
/**
 * @author Turdean Bogdan
 * @since May 12, 2021
 */
import model.Client;

public class ClientAgeValidator implements Validator<Client>{
    private static final int MIN_AGE = 7;
    private static final int MAX_AGE = 30;

    /**
     * Functie pentru care se valideaza varsta unui client
     * @param t clientul pentru care se valideaza varsta
     */
    public void validate(Client t) {

        if (t.getAge() < MIN_AGE || t.getAge() > MAX_AGE) {
            throw new IllegalArgumentException("The Student Age limit is not respected!");
        }

    }
}
