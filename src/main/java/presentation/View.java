package presentation;

/**
 * @author Turdean Bogdan
 * @since May 12, 2021
 */

import model.Client;
import model.Comanda;
import model.Produs;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.lang.reflect.Field;

public class View extends JFrame {
    private JLabel idClient = new JLabel("ID Client");
    private JLabel idProdus = new JLabel("ID Produs");
    private JLabel idComanda = new JLabel("ID Comanda");
    private JLabel numeClient = new JLabel("Nume Client");
    private JLabel numeProdus = new JLabel("Nume Produs");
    private JLabel emailClient = new JLabel("Email Client");
    private JLabel pretProdus = new JLabel("Pret Produs");
    private JLabel varstaClient = new JLabel("Varsta Client");
    private JLabel cantitateProdus = new JLabel("Cantitate Produs");
    private JLabel cantitateDorita = new JLabel("Cantitate dorita");

    private JTextField idClientText = new JTextField(100);
    private JTextField idProdusText = new JTextField(100);
    private JTextField idComandaText = new JTextField(100);
    private JTextField numeClientText = new JTextField(100);
    private JTextField numeProdusText = new JTextField(100);
    private JTextField emailClientText= new JTextField(100);
    private JTextField pretProdusText = new JTextField(100);
    private JTextField varstaClientText = new JTextField(100);
    private JTextField cantitateProdusText = new JTextField(100);
    private JTextField cantitateDoritaText = new JTextField(100);
    private JTextField idClientComandaText = new JTextField(100);
    private JTextField idProdusComandaText = new JTextField(100);

    String[][] data1 = {
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""}
    };

    String[] columnNames1 = {"", "", "", ""};

    String[][] data2 = {
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""}
    };

    String[] columnNames2 = {"", "", "", ""};

    String[][] data3 = {
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""},
            {"", "", "", ""}
    };

    String[] columnNames3 = {"", "", "", ""};

    private JTable client = new JTable(data1, columnNames1);
    private JTable produs = new JTable(data2, columnNames2);
    private JTable comanda = new JTable(data3, columnNames3);

    private JButton clientFindById = new JButton("Find client by id");
    private JButton findAllClients = new JButton("Find all clients");
    private JButton updateClient = new JButton("Update client");
    private JButton insertClient = new JButton("Insert client");
    private JButton deleteClient = new JButton("Delete client");

    private JButton productFindById = new JButton("Find product by id");
    private JButton findAllProduct = new JButton("Find all products");
    private JButton updateProduct = new JButton("Update product");
    private JButton insertProduct = new JButton("Insert product");
    private JButton deleteProduct = new JButton("Delete product");

    private JButton commandFindById = new JButton("Find comand by id");
    private JButton findAllCommands = new JButton("Find all comands");
    private JButton updateCommand = new JButton("Update comand");
    private JButton insertCommand = new JButton("Insert comand");
    private JButton deleteCommand = new JButton("Delete comand");

    /**
     * Constructorul clasei View
     */
    public View(){
        JPanel continut = new JPanel();
        continut.setLayout(new BoxLayout(continut, BoxLayout.X_AXIS));

        JPanel titlu = new JPanel();
        titlu.setLayout(new FlowLayout());
        titlu.add(new JLabel("Order management"));

        JPanel clientFinal = new JPanel();
        clientFinal.setLayout(new BoxLayout(clientFinal, BoxLayout.Y_AXIS));
        clientFinal.add(new JLabel("Client"));

        JPanel client1 = new JPanel();
        client1.setLayout(new BoxLayout(client1, BoxLayout.X_AXIS));
        client1.add(new JLabel("ID CLient"));
        client1.add(idClientText);
        client1.add(clientFindById);

        JPanel client2 = new JPanel();
        client2.setLayout(new BoxLayout(client2, BoxLayout.X_AXIS));
        client2.add(numeClient);
        client2.add(numeClientText);
        client2.add(findAllClients);

        JPanel client3 = new JPanel();
        client3.setLayout(new BoxLayout(client3, BoxLayout.X_AXIS));
        client3.add(emailClient);
        client3.add(emailClientText);
        client3.add(updateClient);

        JPanel client4 = new JPanel();
        client4.setLayout(new BoxLayout(client4, BoxLayout.X_AXIS));
        client4.add(varstaClient);
        client4.add(varstaClientText);
        client4.add(insertClient);

        clientFinal.add(client1);
        clientFinal.add(client2);
        clientFinal.add(client3);
        clientFinal.add(client4);
        clientFinal.add(deleteClient);
        clientFinal.add(client);

        JPanel produsFinal = new JPanel();
        produsFinal.setLayout(new BoxLayout(produsFinal, BoxLayout.Y_AXIS));
        produsFinal.add(new JLabel("Produs"));

        JPanel produs1 = new JPanel();
        produs1.setLayout(new BoxLayout(produs1, BoxLayout.X_AXIS));
        produs1.add(new JLabel("ID Produs"));
        produs1.add(idProdusText);
        produs1.add(productFindById);

        JPanel produs2 = new JPanel();
        produs2.setLayout(new BoxLayout(produs2, BoxLayout.X_AXIS));
        produs2.add(numeProdus);
        produs2.add(numeProdusText);
        produs2.add(findAllProduct);

        JPanel produs3 = new JPanel();
        produs3.setLayout(new BoxLayout(produs3, BoxLayout.X_AXIS));
        produs3.add(pretProdus);
        produs3.add(pretProdusText);
        produs3.add(updateProduct);

        JPanel produs4 = new JPanel();
        produs4.setLayout(new BoxLayout(produs4, BoxLayout.X_AXIS));
        produs4.add(cantitateProdus);
        produs4.add(cantitateProdusText);
        produs4.add(insertProduct);

        produsFinal.add(produs1);
        produsFinal.add(produs2);
        produsFinal.add(produs3);
        produsFinal.add(produs4);
        produsFinal.add(deleteProduct);
        produsFinal.add(produs);

        JPanel comandaFinal = new JPanel();
        comandaFinal.setLayout(new BoxLayout(comandaFinal, BoxLayout.Y_AXIS));
        comandaFinal.add(new JLabel("Comanda"));

        JPanel comanda1 = new JPanel();
        comanda1.setLayout(new BoxLayout(comanda1, BoxLayout.X_AXIS));
        comanda1.add(idComanda);
        comanda1.add(idComandaText);
        comanda1.add(commandFindById);

        JPanel comanda2 = new JPanel();
        comanda2.setLayout(new BoxLayout(comanda2, BoxLayout.X_AXIS));
        comanda2.add(idClient);
        comanda2.add(idClientComandaText);
        comanda2.add(findAllCommands);

        JPanel comanda3 = new JPanel();
        comanda3.setLayout(new BoxLayout(comanda3, BoxLayout.X_AXIS));
        comanda3.add(idProdus);
        comanda3.add(idProdusComandaText);
        comanda3.add(updateCommand);

        JPanel comanda4 = new JPanel();
        comanda4.setLayout(new BoxLayout(comanda4, BoxLayout.X_AXIS));
        comanda4.add(cantitateDorita);
        comanda4.add(cantitateDoritaText);
        comanda4.add(insertCommand);

        comandaFinal.add(comanda1);
        comandaFinal.add(comanda2);
        comandaFinal.add(comanda3);
        comandaFinal.add(comanda4);
        comandaFinal.add(deleteCommand);
        comandaFinal.add(comanda);

        continut.add(clientFinal);
        continut.add(produsFinal);
        continut.add(comandaFinal);

        JPanel finalPanel = new JPanel();
        finalPanel.setLayout(new BoxLayout(finalPanel, BoxLayout.Y_AXIS));
        finalPanel.add(titlu);
        finalPanel.add(continut);

        reset();
        Client c = new Client();
        Produs p = new Produs();
        Comanda co = new Comanda();
        initTable(client, c);
        initTable(produs, p);
        initTable(comanda, co);

        this.setContentPane(finalPanel);
        this.pack();
        this.setTitle("Order management");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    /**
     * Functie ce sterge continutul tuturor casutelor text
     */
    public void reset(){
        this.idClientText.setText("");
        this.idProdusText.setText("");
        this.idComandaText.setText("");
        this.numeClientText.setText("");
        this.numeProdusText.setText("");
        this.emailClientText.setText("");
        this.pretProdusText.setText("");
        this.varstaClientText.setText("");
        this.cantitateProdusText.setText("");
        this.cantitateDoritaText.setText("");
        this.idClientComandaText.setText("");
        this.idProdusComandaText.setText("");
    }

    /**
     * Functie ce initializeaza un tabel(se afiseaza numele coloanelor)
     * @param t tabelul care va fi initializat
     * @param o obiectul a carui continut este afisat in tabel
     */
    public void initTable(JTable t, Object o){
        int i = 0;
        for(Field field: o.getClass().getDeclaredFields()){
            field.setAccessible(true);
            Object value;
            try{
                value = field.get(o);
                t.setValueAt(field.getName(), 0, i);
                i++;
            } catch (IllegalAccessException e){
                e.printStackTrace();
            } catch (IllegalArgumentException e){
                e.printStackTrace();
            }
        }
    }

    /**
     * Functie ce scrie continutul unui obiect intr-un tabel la o anumita linie
     * @param t tabelul unde se scrie
     * @param o obiectul al carui continut este afisat in tabel
     * @param line linia din tabel unde se afiseaza
     */
    public void addTable(JTable t, Object o, int line){
        int i = 0;
        for(Field field: o.getClass().getDeclaredFields()){
            field.setAccessible(true);
            Object value;
            try{
                value = field.get(o);
                t.setValueAt(String.valueOf(value), line, i);
                i++;
            } catch (IllegalAccessException e){
                e.printStackTrace();
            } catch (IllegalArgumentException e){
                e.printStackTrace();
            }
        }
    }

    public void addClientListener(ActionListener mal){
        insertClient.addActionListener(mal);
    }

    public void deleteClientListener(ActionListener mal){
        deleteClient.addActionListener(mal);
    }

    public void updateClientListener(ActionListener mal){
        updateClient.addActionListener(mal);
    }

    public void findAllClientsListener(ActionListener mal){
        findAllClients.addActionListener(mal);
    }

    public void findClientByIdListener(ActionListener mal){
        clientFindById.addActionListener(mal);
    }

    public void addProductListener(ActionListener mal){
        insertProduct.addActionListener(mal);
    }

    public void deleteProductListener(ActionListener mal){
        deleteProduct.addActionListener(mal);
    }

    public void updateProductListener(ActionListener mal){
        updateProduct.addActionListener(mal);
    }

    public void findAllProductsListener(ActionListener mal){
        findAllProduct.addActionListener(mal);
    }

    public void findProductByIdListener(ActionListener mal){
        productFindById.addActionListener(mal);
    }

    public void addCommandListener(ActionListener mal){
        insertCommand.addActionListener(mal);
    }

    public void deleteCommandListener(ActionListener mal){
        deleteCommand.addActionListener(mal);
    }

    public void updateCommandListener(ActionListener mal){
        updateCommand.addActionListener(mal);
    }

    public void findAllCommandsListener(ActionListener mal){
        findAllCommands.addActionListener(mal);
    }

    public void findCommandByIdListener(ActionListener mal){
        commandFindById.addActionListener(mal);
    }

    public JTable getClient() {
        return client;
    }

    public JTable getProdus() {
        return produs;
    }

    public JTable getComanda() {
        return comanda;
    }

    public void setClient(Object o, int line) {
        this.addTable(client, o, line);
    }

    public void setProdus(Object o, int line) {
        this.addTable(produs, o, line);
    }

    public void setComanda(Object o, int line) {
        this.addTable(comanda, o, line);
    }

    public String getIdCLient(){
        return idClientText.getText();
    }

    public String getIdProdus(){
        return idProdusText.getText();
    }

    public String getIdComanda(){
        return idComandaText.getText();
    }

    public String getNumeCLient(){
        return numeClientText.getText();
    }

    public String getEmailCLient(){
        return emailClientText.getText();
    }

    public String getVarstaCLient(){
        return varstaClientText.getText();
    }

    public String getNumeProdus(){
        return numeProdusText.getText();
    }

    public String getPretProdus(){
        return pretProdusText.getText();
    }

    public String getCantitateProdus(){
        return cantitateProdusText.getText();
    }

    public String getIdCLientComanda(){
        return idClientComandaText.getText();
    }

    public String getIdProdusComanda(){
        return idProdusComandaText.getText();
    }

    public String getCantitateComanda(){
        return cantitateDoritaText.getText();
    }

    /**
     * Functie care sterge cntinutul unui tabel
     * @param table tabelul al carui continut va fi sters
     */
    public void resetTable(JTable table){
        for (int i = 2; i < 18; i++)
            for (int j = 0; j < 4; j++)
                table.setValueAt("", i, j);
    }
}
