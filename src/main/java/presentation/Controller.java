package presentation;

/**
 * @author Turdean Bogdan
 * @since May 12, 2021
 */

import bll.ClientBll;
import bll.ComandaBll;
import bll.ProdusBll;
import model.Client;
import model.Comanda;
import model.Produs;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class Controller {
    /**
     * Fereastra unde se vor aplica operatiile
     */
    private View view;

    public Controller(View view) {
        this.view = view;

        this.view.addClientListener(new AddClientListener());
        this.view.findAllClientsListener(new FindAllClientsListener());
        this.view.findClientByIdListener(new FindClientByIdListener());
        this.view.updateClientListener(new UpdateClientListener());
        this.view.deleteClientListener(new DeleteClientListener());

        this.view.addProductListener(new AddProductListener());
        this.view.findAllProductsListener(new FindAllProductsListener());
        this.view.findProductByIdListener(new FindProductByIdListener());
        this.view.updateProductListener(new UpdateProductListener());
        this.view.deleteProductListener(new DeleteProductListener());

        this.view.addCommandListener(new AddCommandListener());
        this.view.findAllCommandsListener(new FindAllCommandsListener());
        this.view.findCommandByIdListener(new FindCommandByIdListener());
        this.view.updateCommandListener(new UpdateCommandListener());
        this.view.deleteCommandListener(new DeleteCommandListener());
    }

    class AddClientListener implements ActionListener{
        /**
         * Functie pentru adaugarea unui client
         * @param e evenimentul produs
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                int id = Integer.parseInt(view.getIdCLient());
                String nume = view.getNumeCLient();
                String email = view.getEmailCLient();
                int age = Integer.parseInt(view.getVarstaCLient());
                view.resetTable(view.getClient());
                view.reset();
                Client newClient = new Client(id, nume, email, age);
                ClientBll clientBll = new ClientBll(view);
                clientBll.insertClient(newClient);
            } catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(view,"Formatul ID-ului sau varstei incorecte");
            }
        }
    }

    class FindAllClientsListener implements ActionListener{

        /**
         * Functie pentru afisarea tabelului Client
         * @param e evenimentul produs
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            view.resetTable(view.getClient());
            ClientBll clientBll = new ClientBll(view);
            List<Client> clients = clientBll.findAllClients();
            int i = 1;
            view.reset();
            for(Client c: clients){
                i++;
                view.setClient(c, i);
            }
        }
    }

    class FindClientByIdListener implements ActionListener{
        /**
         * Functie pentru afisarea unui anumit client
         * @param e evenimentul produs
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            ClientBll clientBll = new ClientBll(view);
            view.resetTable(view.getClient());
            try {
                int id = Integer.parseInt(view.getIdCLient());
                Client client = clientBll.findClientById(id);
                int i = 2;
                view.reset();
                view.setClient(client, i);
            } catch (NumberFormatException ex){
                JOptionPane.showMessageDialog(view, "Formatul ID-ului incorect");
            }

        }
    }

    class UpdateClientListener implements ActionListener{
        /**
         * Functie pentru actualizarea unui client
         * @param e evenimentul produs
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            try{
                int id = Integer.parseInt(view.getIdCLient());
                String nume = view.getNumeCLient();
                String email = view.getEmailCLient();
                int age = Integer.parseInt(view.getVarstaCLient());
                view.resetTable(view.getClient());
                view.reset();
                Client newClient = new Client(nume, email, age);
                ClientBll clientBll = new ClientBll(view);
                clientBll.update(id, newClient);
            } catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(view,"Formatul ID-ului sau varstei incorecte");
            }
        }
    }

    class DeleteClientListener implements ActionListener{
        /**
         * Functie pentru stergerea unui client
         * @param e evenimentul produs
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            ClientBll clientBll = new ClientBll(view);
            ComandaBll comandaBll = new ComandaBll(view);
            List<Comanda> comenzi = comandaBll.findAllCommands();
            view.resetTable(view.getClient());
            try {
                int id = Integer.parseInt(view.getIdCLient());
                for(Comanda c: comenzi){
                    if(c.getIdClient() == id){
                        comandaBll.deleteComandaById(c.getId());
                    }
                }
                clientBll.deleteClientById(id);
                view.reset();
            } catch (NumberFormatException ex){
                JOptionPane.showMessageDialog(view, "Formatul ID-ului incorect");
            }
        }
    }

    class AddProductListener implements ActionListener{
        /**
         * Functie pentru inserarea unui produs nou
         * @param e evenimentul produs
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            view.resetTable(view.getProdus());
            try{
                int id = Integer.parseInt(view.getIdProdus());
                String nume = view.getNumeProdus();
                int price = Integer.parseInt(view.getPretProdus());
                int cantity = Integer.parseInt(view.getCantitateProdus());
                Produs newProduct = new Produs(id, nume, price, cantity);
                view.reset();
                ProdusBll produsBll = new ProdusBll(view);
                produsBll.insertProdus(newProduct);
            } catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(view,"Formatul ID-ului sau cantitatii sau pretului incorecte");
            }
        }
    }

    class FindAllProductsListener implements ActionListener{
        /**
         * Functie pentru afisarea tabelului Produs
         * @param e evenimentul produs
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            ProdusBll produsBll = new ProdusBll(view);
            view.resetTable(view.getProdus());
            List<Produs> products = produsBll.findAllProducts();
            int i = 1;
            view.reset();
            for(Produs p: products){
                i++;
                view.setProdus(p, i);
            }
        }
    }

    class FindProductByIdListener implements ActionListener{
        /**
         * Functie pentru afisarea unui anumit produs
         * @param e evenimentul produs
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            ProdusBll produsBll = new ProdusBll(view);
            view.resetTable(view.getProdus());
            try {
                int id = Integer.parseInt(view.getIdProdus());
                Produs product = produsBll.findProductById(id);
                int i = 2;
                view.reset();
                view.setProdus(product, i);
            } catch (NumberFormatException ex){
                JOptionPane.showMessageDialog(view, "Formatul ID-ului incorect");
            }
        }
    }

    class UpdateProductListener implements ActionListener{
        /**
         * Functie pentru actualizarea unui produs
         * @param e evenimentul produs
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            view.resetTable(view.getProdus());
            try{
                int id = Integer.parseInt(view.getIdProdus());
                String nume = view.getNumeProdus();
                int price = Integer.parseInt(view.getPretProdus());
                int cantity = Integer.parseInt(view.getCantitateProdus());
                Produs newProduct = new Produs(nume, price, cantity);
                view.reset();
                ProdusBll produsBll = new ProdusBll(view);
                produsBll.update(id, newProduct);
            } catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(view,"Formatul ID-ului sau cantitatii sau pretului incorecte");
            }
        }
    }

    class DeleteProductListener implements ActionListener{
        /**
         * Functie pentru stergerea unui produs
         * @param e evenimentul produs
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            ProdusBll produsBll = new ProdusBll(view);
            ComandaBll comandaBll = new ComandaBll(view);
            List<Comanda> comenzi = comandaBll.findAllCommands();
            view.resetTable(view.getProdus());
            try {
                int id = Integer.parseInt(view.getIdProdus());
                for(Comanda c: comenzi){
                    if(c.getIdProdus() == id){
                        comandaBll.deleteComandaById(c.getId());
                    }
                }
                produsBll.deleteProductById(id);
                view.reset();
            } catch (NumberFormatException ex){
                JOptionPane.showMessageDialog(view, "Formatul ID-ului incorect");
            }
        }
    }

    class AddCommandListener implements ActionListener{
        /**
         * Functie pentru adaugarea unei noi comenzi
         * @param e evenimentul produs
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            view.resetTable(view.getComanda());
            try{
                int id = Integer.parseInt(view.getIdComanda());
                int idC = Integer.parseInt(view.getIdCLientComanda());
                int idP = Integer.parseInt(view.getIdProdusComanda());
                int cantity = Integer.parseInt(view.getCantitateComanda());
                Comanda newComanda = new Comanda(id, idC, idP, cantity);
                view.reset();
                ComandaBll comandaBll = new ComandaBll(view);
                comandaBll.insertComanda(newComanda);
            } catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(view,"Formatul ID-urilor sau cantitatii incorecte");
            }
        }
    }

    class FindAllCommandsListener implements ActionListener{
        /**
         * Functie pentru afisarea tabelului Comanda
         * @param e evenimentul produs
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            ComandaBll comandaBll = new ComandaBll(view);
            view.resetTable(view.getComanda());
            List<Comanda> commands = comandaBll.findAllCommands();
            int i = 1;
            view.reset();
            for(Comanda c: commands){
                i++;
                view.setComanda(c, i);
            }
        }
    }

    class FindCommandByIdListener implements ActionListener{
        /**
         * Functie pentru afisarea unei anumite comenzi
         * @param e evenimentul produs
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            ComandaBll comandaBll = new ComandaBll(view);
            view.resetTable(view.getComanda());
            try {
                int id = Integer.parseInt(view.getIdComanda());
                Comanda command = comandaBll.findComandaById(id);
                int i = 2;
                view.reset();
                view.setComanda(command, i);
            } catch (NumberFormatException ex){
                JOptionPane.showMessageDialog(view, "Formatul ID-ului incorect");
            }
        }
    }

    class UpdateCommandListener implements ActionListener{
        /**
         * Functie pentru actualizarea unei comenzi
         * @param e evenimentul produs
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            view.resetTable(view.getComanda());
            try{
                int id = Integer.parseInt(view.getIdComanda());
                int idC = Integer.parseInt(view.getIdCLientComanda());
                int idP = Integer.parseInt(view.getIdProdusComanda());
                int cantity = Integer.parseInt(view.getCantitateComanda());
                Comanda newComanda = new Comanda(idC, idP, cantity);
                view.reset();
                ComandaBll comandaBll = new ComandaBll(view);
                comandaBll.update(id, newComanda);
            } catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(view,"Formatul ID-urilor sau cantitatii incorecte");
            }
        }
    }

    class DeleteCommandListener implements ActionListener{
        /**
         * Functie pentru stergerea unei comenzi
         * @param e evenimentul produs
         */
        @Override
        public void actionPerformed(ActionEvent e) {
            ComandaBll comandaBll = new ComandaBll(view);
            view.resetTable(view.getComanda());
            try {
                int id = Integer.parseInt(view.getIdComanda());
                comandaBll.deleteComandaById(id);
                view.reset();
            } catch (NumberFormatException ex){
                JOptionPane.showMessageDialog(view, "Formatul ID-ului incorect");
            }
        }
    }
}
