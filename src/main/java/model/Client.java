package model;

/**
 * @author Turdean Bogdan
 * @since May 12, 2021
 *
 */

public class Client {
    private int id;
    private String name;
    private String email;
    private int age;

    public Client(){

    }

    /**
     * Constructor cu toate campurile pentru a executa insert
     * @param id identificatorul unic al clientului
     * @param name numele clientului
     * @param email adresa de email a clientului
     * @param age varsta clientului
     */

    public Client(int id, String name, String email, int age) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.age = age;
    }

    /**
     * Constructor fara campul ID pentru a executa update
     * @param name noul nume
     * @param email noul email
     * @param age noua varsta
     */

    public Client(String name, String email, int age) {
        this.name = name;
        this.email = email;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Client [id=" + id + ", name=" + name + ", email=" + email + ", age=" + age
                + "]";
    }
}
