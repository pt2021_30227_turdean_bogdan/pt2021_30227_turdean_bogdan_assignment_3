package model;

/**
 * @author Turdean Bogdan
 * @since May 12, 2021
 */

public class Comanda {
    /**
     * identificatorul comenzii
     */
    private int id;
    /**
     * identificatorul clientului care plaseaza comanda
     */
    private int idClient;
    /**
     * identificatorul produsului comandat
     */
    private int idProdus;
    /**
     * cantitatea produsului
     */
    private int cantitate;

    /**
     * Constructor cu toate campurile pentru insert
     * @param id
     * @param idClient
     * @param idProdus
     * @param cantitate
     */
    public Comanda(int id, int idClient, int idProdus, int cantitate) {
        this.id = id;
        this.idClient = idClient;
        this.idProdus = idProdus;
        this.cantitate = cantitate;
    }

    /**
     * Constructor fara campul ID pentru a executa update
     * @param idClient identificatorul noului client
     * @param idProdus identificatorul noului produs
     * @param cantitate noua cantitate comandata
     */
    public Comanda(int idClient, int idProdus, int cantitate) {
        this.idClient = idClient;
        this.idProdus = idProdus;
        this.cantitate = cantitate;
    }

    public Comanda() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }

    public int getIdProdus() {
        return idProdus;
    }

    public void setIdProdus(int idProdus) {
        this.idProdus = idProdus;
    }

    public int getCantitate() {
        return cantitate;
    }

    public void setCantitate(int cantitate) {
        this.cantitate = cantitate;
    }
}
