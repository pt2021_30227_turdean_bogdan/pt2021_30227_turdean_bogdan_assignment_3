package model;

/**
 * @author Turdean Bogdan
 * @since May 12, 2021
 */

public class Produs {
    private int id;
    private String name;
    private int price;
    private int cantity;

    /**
     * Constructor cu toti parametrii pentru insert
     * @param id identificatorul unic
     * @param name numele
     * @param price pretul
     * @param cantity cantitatea(stocul)
     */
    public Produs(int id, String name, int price, int cantity) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.cantity = cantity;
    }

    /**
     * Constructor fara parametrul ID pentru a executa update
     * @param name noul nume
     * @param price noul pret
     * @param cantity noua cantitate
     */
    public Produs(String name, int price, int cantity) {
        this.name = name;
        this.price = price;
        this.cantity = cantity;
    }

    public Produs() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getCantity() {
        return cantity;
    }

    public void setCantity(int cantity) {
        this.cantity = cantity;
    }
}
